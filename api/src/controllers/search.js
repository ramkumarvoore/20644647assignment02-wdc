const express = require('express');
const _ = require('lodash');
const models = require('../models');
const router = express.Router();

router.get('/:searchValue', (req, res) => {
    let allNotes = [], allNotebooks = [];
    let finalData = [];
    let optionsTitle = {
        where: {
            title: { $like: '%' + req.params.searchValue + '%' }
        }
    };
    let optionsContent = {
        where: {
            content: { $like: '%' + req.params.searchValue + '%' }
        }
    };
    models.Note.findAll(optionsTitle)
        .then(notes => notes.map(note => allNotes.push(note)))
        .then(notes => models.Note.findAll(optionsContent))
        .then(notes => allNotes = [...new Set([...allNotes, ...notes])])
        .then(notes => models.Notebook.findAll(optionsTitle))
        .then(notebooks => notebooks.map(notebook => allNotebooks.push(notebook)))
        .then(data => {
            finalData = allNotebooks.concat(allNotes);
            res.json(finalData);
        })
        .catch(err => res.status(500).json({ error: err.message }));
});

module.exports = router;