const express = require('express');
const _ = require('lodash');
const models = require('../models');

const router = express.Router();

/* *** TODO: Fill in the API endpoints for notes *** */
router.get('/', (req, res) => {
    models.Note.findAll({ order: [['createdAt', 'DESC']] })
      .then(notes => res.json(notes))
      .catch(err => res.status(500).json({ error: err.message }));
  });
  
  router.post('/', (req, res) => {
    models.Note.create({ title: req.body.title, content: req.body.content, notebookId: req.body.notebookId })
      .then(note => res.json(note))
      .catch(err => res.status(500).json({ error: err.message }));
  });
  
  router.get('/:noteId', (req, res) => {
    models.Note.findOne({ id: req.params.noteId })
      .then(note => res.json(note))
      .catch(err => res.status(500).json({ error: err.message }));
  });
  
  router.put('/:noteId', (req, res) => {
    models.Note.update({ title: req.body.title, content: req.body.content, notebookId: req.body.notebookId  }, { where: { id: req.params.noteId } })
      .then(note => res.json(req.body))
      .catch(err => res.status(500).json({ error: err.message }));
  });
  
  router.delete('/:noteId', (req, res) => {
    models.Note.destroy({ where: { id: req.params.noteId } })
      .then(note => res.json({}))
      .catch(err => res.status(500).json({ error: err.message }));
  });

module.exports = router;
