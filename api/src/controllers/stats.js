const express = require('express');
const _ = require('lodash');
const models = require('../models');

const router = express.Router();

// Index
router.get('/', (req, res) => {
    let notebooks, notes;
    models.Notebook.findAll({ order: [['createdAt', 'ASC']] })
        .then(nbs => {
            notebooks = nbs;
            return models.Note.findAll()
                .then(notess => {
                    notes = notess;
                    const obj = {};
                    obj.noteCount = notes.length;
                    obj.notebookCount = notebooks.length;
                    obj.oldestNotebook = notebooks.length > 0 ? notebooks[0].title : '';
                    obj.recentlyUpdatedNote = notes.length ? notes[0].title : '';
                    return res.json(obj);
                })
        })
        .catch(err => res.status(500).json({ error: err.message }));
});


module.exports = router;
