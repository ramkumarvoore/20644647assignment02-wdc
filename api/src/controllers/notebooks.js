const express = require('express');
const _ = require('lodash');
const models = require('../models');

const router = express.Router();

// Index
router.get('/', (req, res) => {
  models.Notebook.findAll({ order: [['createdAt', 'DESC']] })
    .then(notebooks => res.json(notebooks))
    .catch(err => res.status(500).json({ error: err.message }));
});

/* *** TODO: Fill in the API endpoints for notebooks *** */

router.post('/', (req, res) => {
  models.Notebook.create({ title: req.body.title })
    .then(notebook => res.json(notebook))
    .catch(err => res.status(500).json({ error: err.message }));
});

router.get('/:notebookId', (req, res) => {
  models.Notebook.findOne({ id: req.params.notebookId, })
    .then(notebook => res.json(notebook))
    .catch(err => res.status(500).json({ error: err.message }));
});

router.put('/:notebookId', (req, res) => {
  models.Notebook.update({ title: req.body.title }, { where: { id: req.params.notebookId } })
    .then(notebook => res.json(req.body))
    .catch(err => res.status(500).json({ error: err.message }));
});

router.delete('/:notebookId', (req, res) => {
  models.Note.destroy({ where: { notebookId: req.params.notebookId } })
    .then(note => models.Notebook.destroy({ where: { id: req.params.notebookId } })
      .then(notebook => res.json({})))
    .catch(err => res.status(500).json({ error: err.message }));
});

router.get('/:notebookId/notes', (req, res) => {
  models.Note.findAll({ order: [['createdAt', 'DESC']], where: { notebookId: req.params.notebookId } })
    .then(notebooks => res.json(notebooks))
    .catch(err => res.status(500).json({ error: err.message }));
});



module.exports = router;
