/**
 * This file contains the Home component.
 * Other React components for viewing notes and notebooks should be nested
 * beneath the Home component.
 */

const React = require('react');

const NotebookList = require('./NotebookList');

/*
  *** TODO: Start building the frontend from here ***
  You should remove the placeholder text and modify the component as you see
  fit while working on the assignment.
*/
const Home = () => (
  <div>
    <div className="app-header">
      <h1 className="app-title">Neverwrote Application</h1>
      <p className="lead app-description">This is the Home component!</p>
    </div>
    <NotebookList />
  </div>
);

module.exports = Home;
