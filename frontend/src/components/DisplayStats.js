const React = require('react');

class DisplayStats extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <ol>
                <li>noteCount - {this.props.stats.noteCount}</li>
                <li>notebookCount - {this.props.stats.notebookCount}</li>
                <li>oldestNotebook - {this.props.stats.oldestNotebook}</li>
                <li>recentlyUpdatedNote - {this.props.stats.recentlyUpdatedNote}</li>
            </ol>
        );
    }
}


module.exports = DisplayStats;