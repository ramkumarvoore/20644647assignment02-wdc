const React = require('react');
const ReactRedux = require('react-redux');

const createActionDispatchers = require('../helpers/createActionDispatchers');
const notebooksActionCreators = require('../reducers/notebooks');
const notesActionCreators = require('../reducers/notes');
const statsActionCreators = require('../reducers/stats');
const ActiveNotebook = require('./ActiveNotebook');
const Notebook = require('./Notebook');
const DisplayStats = require('./DisplayStats');
const NotebookNew = require('./NotebookNew');
const NoteNew = require('./NoteNew');
/*
  *** TODO: Build more functionality into the NotebookList component ***
  At the moment, the NotebookList component simply renders the notebooks
  as a plain list containing their titles. This code is just a starting point,
  you will need to build upon it in order to complete the assignment.
*/
class NotebookList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isStatsClicked: false, isNewNotebookActive: false, isNewNoteActive: false, clinetSerach: '', serverSearch: '', notebooks: this.props.notebooks.notebooks };
  }

  componentDidMount() {
    this.props.loadNotebooks(() => {
      this.setState({ notebooks: this.props.notebooks.notebooks })
    });
  }

  render() {
    const getStats = () => {
      this.setState({ isStatsClicked: true });
      this.props.loadStats();
    }
    const onClinetSerachChange = (event) => {
      this.setState({ clinetSerach: event.target.value });
    };
    const onServerSerachChange = (event) => {
      this.setState({ serverSearch: event.target.value });
    };
    const onClinetSerach = () => {
      const { notebooks } = this.props.notebooks;
      const { clinetSerach } = this.state;
      let searchedNotebooks = [];
      if (this.state.clinetSerach != '') {
        searchedNotebooks = notebooks.filter(notebook => notebook.title.includes(clinetSerach))
        this.setState({ notebooks: searchedNotebooks })
      } else {
        this.setState({ notebooks: notebooks })
      }
    }
    const onServerSerach = () => {
      this.props.searchAll(this.state.serverSearch);
    }
    const deleteNotes = (noteId) => {
      this.props.deleteNote(noteId);
    }
    const onDeleteNotebook = (notebookId) => {
      this.props.deleteNotebook(notebookId, () => {
        this.setState({ notebooks: this.props.notebooks.notebooks });
      });
    }
    const onDeleteNote = (noteId) => {
      this.props.deleteNote(noteId, ()=> {
        this.props.loadNotesFromNotebook(this.props.notebooks.activeNoteId);
      });
    }

    const createNotebookComponent = (notebook) => {
      if (notebook.id === this.props.notebooks.activeNotebookId) {
        return <ActiveNotebook
          key={notebook.id}
          notebook={notebook}
          notes={this.props.notebooks.notes}
          activeNoteId={this.props.notebooks.activeNoteId}
          setActiveNoteId={this.props.setActiveNoteId}
          deleteNotes={deleteNotes}
          onDeleteNotebook={onDeleteNotebook}
          onDeleteNote={onDeleteNote}
        />
      }
      return <Notebook
        key={notebook.id}
        notebook={notebook}
        loadNotesFromNotebook={this.props.loadNotesFromNotebook} />;
    };
    const createSearchValuesComponent = (data) => {
      return (
        <ul>
          Title : {data.title} ------ Content : {data.content ? data.content : ''} ------ Created At : {data.createdAt} ------ Updated At : {data.updatedAt}
        </ul>
      );
    }

    const setNotebookActive = () => {
      this.setState({isNewNotebookActive: true, isNewNoteActive: false})
    }
    const setNoteActive = () => {
      this.setState({isNewNotebookActive: false, isNewNoteActive: true})
    }
    const saveEdit = () => {
      this.setState({ notebooks: this.props.notebooks.notebooks })
    }
    const closeEdit = () => {
      this.setState({isNewNotebookActive: false, isNewNoteActive: false})
    }
    return (
      <div>
        <button className="button btn btn-primary btn-sm" onClick={setNotebookActive}>Add new notebook</button>
        <button className="button btn btn-primary btn-sm" onClick={setNoteActive}>Add new note</button>
        <form style={{ margin: 'auto', maxWidth: 200 }}>
          <div className="input-group">
            <input placeholder="search for notebooks" className="form-control" value={this.state.clinetSerach} onChange={onClinetSerachChange} />
            <span className="input-group-btn">
              <button type = "button" className="btn btn-info" onClick={onClinetSerach}><i title="client side" className="fa fa-search"></i></button>
            </span>
          </div>
        </form>
        {this.state.isNewNotebookActive && <NotebookNew saveEdit = {saveEdit} closeEdit={closeEdit} createNotebook={this.props.createNotebook}/>}
        {this.state.isNewNoteActive && <NoteNew closeEdit={closeEdit} createNote={this.props.createNote}/>}
        <ul>
          <h3>Notebooks</h3>
          {this.state.notebooks && this.state.notebooks.map(createNotebookComponent)}
        </ul>
        <button className="button btn btn-primary btn-sm" onClick={getStats}>Get Statistics</button>
        <button className="button btn btn-primary btn-sm" onClick={getStats} disabled={!this.state.isStatsClicked}>Refresh Statistics</button>
        {this.state.isStatsClicked && (<DisplayStats stats={this.props.stats.stats} />)}
        <form style={{ margin: 'auto', maxWidth: 200 }}>
          <div className="input-group">
            <input placeholder="search for notes and notesbook" className="form-control" value={this.state.serverSearch} onChange={onServerSerachChange} />
            <span className="input-group-btn">
              <button type = "button" className="btn btn-info" onClick={onServerSerach}><i title="server side" className="fa fa-search"></i></button>
            </span>
          </div>
        </form>
        {<div>
          <h3>Notebooks & Notes from server side search</h3>
          <ul>
            {this.props.notebooks.searchedData && this.props.notebooks.searchedData.map(createSearchValuesComponent)}
          </ul>

        </div>}
      </div>

    );
  }
}

const NotebookListContainer = ReactRedux.connect(
  state => ({
    notebooks: state.notebooks,
    notes: state.notes,
    stats: state.stats
  }),
  createActionDispatchers(notebooksActionCreators, notesActionCreators, statsActionCreators)
)(NotebookList);

module.exports = NotebookListContainer;
