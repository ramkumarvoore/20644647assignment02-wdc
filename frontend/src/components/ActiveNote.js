const React = require('react');

class ActiveNote extends React.Component {
    render() {
        const onDelete = () => {
            this.props.onDeleteNote(this.props.note.id);
        }
        return (
            <div>
                <a role="button" title={'Delete '+ this.props.note.title}
                    style={{ paddingRight: '8px' }}
                    onClick={onDelete}
                >
                    <span className="fa fa-remove" />
                </a>
                <li>
                    {this.props.note.title}
                    <ol>
                        <li>Title - {this.props.note.title}</li>
                        <li>Content - {this.props.note.content ? this.props.note.content : ''}</li>
                        <li>Created At - {this.props.note.createdAt}</li>
                    </ol>
                </li>
            </div>

        );
    }
}

module.exports = ActiveNote;