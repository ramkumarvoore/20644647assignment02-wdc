const React = require('react');

class NoteNew extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            content: '',
            id: ''
        }
    }
    render() {
        const onTitleChange = (event) => {
            this.setState({ title: event.target.value });
        };
        const onContentChange = (event) => {
            this.setState({ content: event.target.value });
        };
        const onIdChange = (event) => {
            this.setState({ id: event.target.value });
        };
        const submitNote = (event) => {
            event.preventDefault();
            const note = Object.assign({}, {
                title: this.state.title,
                content: this.state.content,
                notebookId: this.state.id
            });
            this.props.createNote(note);
            this.props.closeEdit();
        };
        const cancelNote = (event) => {
            event.preventDefault();
            this.props.closeEdit();
        };
        return (
            <form className="app-post">
                {/* Title field */}
                <div className="form-group">
                    <input className="form-control input-lg" value={this.state.title}
                        placeholder="Note title" onChange={onTitleChange}
                    />
                </div>
                <div className="form-group">
                    <input className="form-control input-lg" value={this.state.content}
                        placeholder="Note content" onChange={onContentChange}
                    />
                </div>
                <div className="form-group">
                    <input className="form-control input-lg" value={this.state.id}
                        placeholder="Notebook Id" onChange={onIdChange}
                    />
                </div>
                {/* Save button */}
                <button className="btn btn-default pull-right"
                    onClick={submitNote}
                >
                    Save
                  </button>
                {/* Cancel button */}
                <button className="btn btn-default pull-right"
                    style={{ marginRight: '12px' }}
                    onClick={cancelNote}
                >
                    Cancel
                  </button>
            </form>
        );
    }
}


module.exports = NoteNew;