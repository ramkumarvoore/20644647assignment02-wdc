const React = require('react');

class NotebookNew extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
        }
    }
    render() {
        const onTitleChange = (event) => {
            this.setState({ title: event.target.value });
        };
        const submitNotebook = (event) => {
            event.preventDefault();
            const notebbok = Object.assign({}, {
                title: this.state.title
            });
            this.props.createNotebook(notebbok, () => {
                this.props.saveEdit()
            });
            this.props.closeEdit();
        };
        const cancelNotebook = (event) => {
            event.preventDefault();
            this.props.closeEdit();
        };
        return (
            <form className="app-post">
                {/* Title field */}
                <div className="form-group">
                    <input className="form-control input-lg" value={this.state.title}
                        placeholder="Notebook title" onChange={onTitleChange}
                    />
                </div>
                {/* Save button */}
                <button className="btn btn-default pull-right"
                    onClick={submitNotebook}
                >
                    Save
                  </button>
                {/* Cancel button */}
                <button className="btn btn-default pull-right"
                    style={{ marginRight: '12px' }}
                    onClick={cancelNotebook}
                >
                    Cancel
                  </button>
            </form>
        );
    }
}


module.exports = NotebookNew;