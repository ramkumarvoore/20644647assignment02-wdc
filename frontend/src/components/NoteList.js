const React = require('react');
const ReactRedux = require('react-redux');

const createActionDispatchers = require('../helpers/createActionDispatchers');
const notesActionCreators = require('../reducers/notes');
const ActiveNotebook = require('./ActiveNotebook');
const Notebook = require('./Notebook');

/*
  *** TODO: Build more functionality into the NotebookList component ***
  At the moment, the NotebookList component simply renders the notebooks
  as a plain list containing their titles. This code is just a starting point,
  you will need to build upon it in order to complete the assignment.
*/
class NoteList extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.loadNotebooks();
  }

  render() {
  	const createNoteComponent = (note) => {
    	if(notebook.id === this.props.notebooks.activeNotebookId) {
      	return <ActiveNotebook key={notebook.id} notebook={notebook} notes={this.props.notebooks.notes}/>;
      }
      return <Notebook key={notebook.id} notebook={notebook} loadNotes={this.props.loadNotes} />;
    };

    return (
    	<ul>
        {this.props.notebooks.notes.map(createNoteComponent)}
      </ul>
    );
  }
}

const NoteListContainer = ReactRedux.connect(
  state => ({
    notes: state.notes
  }),
  createActionDispatchers(notesActionCreators)
)(NoteList);

module.exports = NoteListContainer;
