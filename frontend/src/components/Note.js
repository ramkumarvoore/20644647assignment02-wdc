const React = require('react');

class Note extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const onClickNote = (event) => {
            event.preventDefault();
            this.props.setActiveNoteId(this.props.note.id);
        };

        return (
            <li>
                <a href="#" onClick={onClickNote}>
                    {this.props.note.title}
                </a>
            </li>
        );
    }
}


module.exports = Note;