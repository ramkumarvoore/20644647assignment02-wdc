const React = require('react');
const ActiveNote = require('./ActiveNote');
const Note = require('./Note');

class ActiveNotebook extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const createNoteComponent = (note) => {
            if (note.id === this.props.activeNoteId) {
                return <ActiveNote key={note.id} note={note} onDeleteNote={this.props.onDeleteNote}/>;
            }
            return <Note key={note.id} note={note} setActiveNoteId={this.props.setActiveNoteId} />;
        };
        const onDelete = () => {
            this.props.onDeleteNotebook(this.props.notebook.id);
        }

        return (
            <div>
                <a role="button" title={'Delete '+ this.props.notebook.title}
                    style={{ paddingRight: '8px' }}
                    onClick={onDelete}
                >
                    <span className="fa fa-remove" />
                </a>
                <li>
                    {this.props.notebook.title}
                    <ol>
                        {this.props.notes.map(createNoteComponent)}
                    </ol>
                </li>
            </div>


        );
    }
}

module.exports = ActiveNotebook;