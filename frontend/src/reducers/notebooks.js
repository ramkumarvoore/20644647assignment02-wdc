const _ = require('lodash');
const api = require('../helpers/api');

// Action type constants

const NOTEBOOK_INSERT = 'Neverwrote-frontend/notebooks/NOTEBOOK_INSERT';
const NOTEBOOK_REMOVE = 'Neverwrote-frontend/notebooks/NOTEBOOK_REMOVE';
const NOTEBOOK_LOAD = 'Neverwrote-frontend/notebooks/NOTEBOOK_LOAD';
const NOTE_LOAD = 'Neverwrote-frontend/notes/NOTE_LOAD';
const NOTE_ACTIVE = 'Neverwrote-frontend/notes/NOTE_ACTIVE';
const SEARCH_DATA = 'Neverwrote-frontend/notes/SEARCH_DATA';

const initialState = {
  notebooks: [],
  activeNotebookId: -1,
  notes: [],
  activeNoteId: -1,
  searchedData: []
};

// Function which takes the current data state and an action,
// and returns a new state
function reducer(state, action) {
  state = state || initialState;
  action = action || {};

  switch (action.type) {
    case NOTEBOOK_REMOVE: {
      const notebooks = _.reject(state.notebooks, { id: action.id });
      return _.assign({}, state, { notebooks });
    }
    case NOTEBOOK_INSERT: {
      const notebooks = _.concat(action.notebook, state.notebooks);
      return _.assign({}, state, { notebooks });
    }
    case NOTEBOOK_LOAD: {
      const notebooks = _.concat(state.notebooks, action.notebooks);
      return _.assign({}, state, { notebooks });
    }
    case NOTE_LOAD: {
      return Object.assign({}, state, { activeNotebookId: action.notebookId, notes: action.notes });
    }
    case NOTE_ACTIVE: {
      return Object.assign({}, state, { activeNoteId: action.noteId });
    }
    case SEARCH_DATA: {
      return Object.assign({}, state, { searchedData: action.data });
    }
    default: return state;
  }
}

reducer.insertNotebook = (notebook) => {
  return { type: NOTEBOOK_INSERT, notebook };
};
reducer.createNotebook = (notebook,cb) => {
  return (dispatch) => {
    api.post('/notebooks/', notebook).then(notebook => {
      dispatch(reducer.insertNotebook([notebook]));
      cb();
    })
  };
};

reducer.removeNotebook = (id) => {
  return { type: NOTEBOOK_REMOVE, id };
};
reducer.deleteNotebook = (notebookId, cb) => {
  return (dispatch) => {
    api.delete('/notebooks/' + notebookId).then(() => {
      dispatch(reducer.removeNotebook(notebookId));
      cb();
    })
  };
};

reducer.serchNotebooks = (notebooks) => {
  return { type: NOTEBOOK_LOAD, notebooks };
};
reducer.loadNotebooks = (cb) => {
  return (dispatch) => {
    api.get('/notebooks').then((notebooks) => {
      dispatch(reducer.serchNotebooks(notebooks));
      cb();
    })
  };
};

reducer.searchNotesFromNotebook = (notes, notebookId) => {
  return { type: NOTE_LOAD, notes, notebookId };
};
reducer.loadNotesFromNotebook = (notebookId) => {
  return (dispatch) => {
    api.get('/notebooks/' + notebookId + '/notes').then((notes) => {
      dispatch(reducer.searchNotesFromNotebook(notes, notebookId));
    })
  };
};

reducer.setActiveNoteId = (noteId) => {
  return (dispatch) => {
    dispatch({ type: NOTE_ACTIVE, noteId });
  };
}

reducer.searchAll = (searchValue) => {
  return (dispatch) => {
    api.get('/search/' + searchValue).then((data) => {
      dispatch({ type: SEARCH_DATA, data });
    })
  };
}
// Export the action creators and reducer
module.exports = reducer;
