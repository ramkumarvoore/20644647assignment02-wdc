const _ = require('lodash');
const api = require('../helpers/api');

// Action type constants

const STATS_LOAD = 'Neverwrote-frontend/notes/STATS_LOAD';

const initialState = {
    stats: { "noteCount": 0, "notebookCount": 0, "oldestNotebook": "", "recentlyUpdatedNote": "" }
};

function reducer(state, action) {
    state = state || initialState;
    action = action || {};

    switch (action.type) {
        case STATS_LOAD: {
            return Object.assign({}, state, { stats: action.stats });
        }
        default: return state;
    }
}

reducer.searchStats = (stats) => {
    return { type: STATS_LOAD, stats };
};
reducer.loadStats = () => {
    return (dispatch) => {
        api.get('/stats').then((stats) => {
            dispatch(reducer.searchStats(stats));
        });
    };
};

// Export the action creators and reducer
module.exports = reducer;
