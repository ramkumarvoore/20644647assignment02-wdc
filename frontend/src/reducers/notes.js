const _ = require('lodash');
const api = require('../helpers/api');

// Action type constants

const NOTE_INSERT = 'Neverwrote-frontend/notes/NOTE_INSERT';
const NOTE_REMOVE = 'Neverwrote-frontend/notes/NOTE_REMOVE';
const NOTE_LOAD = 'Neverwrote-frontend/notes/NOTE_LOAD';
const NOTE_CLICKED = 'Neverwrote-frontend/notes/NOTE_CLICKED';

const initialState = {
    notes: [
        { id: 200, title: 'From Redux Store: A hard-coded note', notebookId: 100 },
        { id: 201, title: 'From Redux Store: Another hard-coded note', notebookId: 101 },
    ],
    activeNoteId:-1
};

function reducer(state, action) {
    state = state || initialState;
    action = action || {};

    switch (action.type) {
        case NOTE_INSERT: {
            const notes = _.concat( action.note, state.notes);
            return _.assign({}, state, { notes });
        }
        case NOTE_REMOVE: {
            const notes = _.reject(state.notes, { id: action.id });
            return _.assign({}, state, { notes });
        }
        case NOTE_LOAD: {
            const notes = _.concat(state.notes, action.notes);
            return _.assign({}, state, { notes });
        }
        case NOTE_CLICKED: {
            return Object.assign({}, state, { activeNoteId: action.noteId });
        }

        default: return state;
    }
}

reducer.insertNote = (note) => {
    return { type: NOTE_INSERT, note };
};
reducer.createNote = (note) => {
    return (dispatch) => {
        api.post('/notes/', note).then((note) => {
            dispatch(reducer.insertNote([note]));
        }).catch(() => {
            alert('Failed to insert note.');
        });
    };
};

reducer.removeNote = (id) => {
    return { type: NOTE_REMOVE, id };
};
reducer.deleteNote = (noteId, cb) => {
    return (dispatch) => {
        api.delete('/notes/' + noteId).then(() => {
            dispatch(reducer.removeNote(noteId));
            cb();
        }).catch(() => {
            alert('Failed to delete note.');
        });
    };
};

reducer.searchNotes = (notes) => {
    return { type: NOTE_LOAD, notes };
};
reducer.loadNotes = () => {
    return (dispatch) => {
        api.get('/notes').then((notes) => {
            dispatch(reducer.searchNotes(notes));
        }).catch(() => {
            alert('Failed to load notes');
        });

    };
};

reducer.retrieveNote = (noteId) => {
    return (dispatch) => {
        dispatch({ type: NOTE_CLICKED, noteId });
    };
}
// Export the action creators and reducer
module.exports = reducer;
